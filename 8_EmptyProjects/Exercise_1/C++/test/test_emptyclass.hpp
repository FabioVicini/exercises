#ifndef __TEST_EMPTYCLASS_H
#define __TEST_EMPTYCLASS_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "EmptyClass.hpp"

using namespace EmptyNamespace;
using namespace testing;
using namespace std;

namespace EmptyTesting {

  TEST(TestEmptyClass, TestEmptyMethod)
  {
    EmptyClass emptyObject;

    try
    {
      Eigen::Vector3d test(1.0, 2.0, 6.0);
      EXPECT_EQ(test.x(), 1.0);
      EXPECT_EQ(test.y(), 2.0);
      EXPECT_EQ(test.z(), 6.0);

      int nonconstInput1, nonconstInput2;
      ASSERT_NO_THROW(emptyObject.EmptyMethod(nonconstInput1, nonconstInput2));
      EXPECT_EQ(nonconstInput1, 4);
      EXPECT_EQ(nonconstInput2, 6);
    }
    catch (const exception& exception)
    {
      FAIL();
    }
  }

  class MockEmptyClass : public IEmptyClass {
    public:
      MOCK_METHOD2(EmptyMethod, bool(int& nonconstInput1, int& nonconstInput2));
  };

  TEST(TestDependentClass, TestEmptyMethod)
  {
    MockEmptyClass mockEmptyClass;
    DependentClass dependentClass(mockEmptyClass);

    try
    {
      EXPECT_CALL(mockEmptyClass, EmptyMethod(_, _)).WillOnce(DoAll(testing::SetArgReferee<0>(5),
                                                                    testing::SetArgReferee<1>(7),
                                                                    Return(true)));
      EXPECT_TRUE(dependentClass.TestMethod());
    }
    catch (const exception& exception)
    {
      FAIL();
    }
  }
}

#endif // __TEST_EMPTYCLASS_H
