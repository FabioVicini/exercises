#include "EmptyClass.hpp"

namespace EmptyNamespace {

  EmptyClass::EmptyClass()
  {

  }
  EmptyClass::~EmptyClass()
  {

  }

  bool EmptyClass::EmptyMethod(int& nonconstInput1, int& nonconstInput2)
  {
    nonconstInput1 = 4;
    nonconstInput2 = 6;
    return true;
  }

  DependentClass::DependentClass(IEmptyClass& emptyClass) :
    _emptyClass(emptyClass)
  {

  }
  DependentClass::~DependentClass()
  {

  }

  bool DependentClass::TestMethod()
  {
    int nonconstInput1 = 0;
    int nonconstInput2 = 0;
    bool result = _emptyClass.EmptyMethod(nonconstInput1, nonconstInput2);

    return result && nonconstInput1 == 5 && nonconstInput2 == 7;
  }

}
