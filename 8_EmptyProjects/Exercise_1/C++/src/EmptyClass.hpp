#ifndef EMPTYCLASS_H
#define EMPTYCLASS_H

namespace EmptyNamespace {

  class IEmptyClass {
    public:
      virtual ~IEmptyClass() {}
      virtual bool EmptyMethod(int& nonconstInput1, int& nonconstInput2) = 0;
  };

  class EmptyClass final : public IEmptyClass {
    public:
      EmptyClass();
      ~EmptyClass();

      bool EmptyMethod(int& nonconstInput1, int& nonconstInput2);
  };

  class DependentClass {
    private:
      IEmptyClass& _emptyClass;

    public:
      DependentClass(IEmptyClass& emptyClass);
      ~DependentClass();

      bool TestMethod();
  };
}

#endif // EMPTYCLASS_H
