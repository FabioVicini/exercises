#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      Point(const double& x,
            const double& y) { }
      Point(const Point& point) { }
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b) { }

      double Area() const { return 0.0; }
  };

  class Circle : public IPolygon
  {
    public:
      Circle(const Point& center,
             const int& radius) { }

      double Area() const { return 0.0; }
  };


  class Triangle : public IPolygon
  {
    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3) { }

      double Area() const { return 0.0; }
  };


  class TriangleEquilateral : public IPolygon
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge) { }

      double Area() const { return 0.0; }
  };

  class Quadrilateral : public IPolygon
  {
    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4) { }

      double Area() const { return 0.0; }
  };


  class Parallelogram : public IPolygon
  {
    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4) { }

      double Area() const { return 0.0; }
  };

  class Rectangle : public IPolygon
  {
    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height) { }

      double Area() const { return 0.0; }
  };

  class Square: public IPolygon
  {
    public:
      Square(const Point& p1,
             const int& edge) { }

      double Area() const { return 0.0; }
  };
}

#endif // SHAPE_H
