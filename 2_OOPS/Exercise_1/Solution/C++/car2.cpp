#include "car2.h"

namespace CarLibrary2 {
  Car::Car(const string& producer, const string& model, const string& color)
  {
    _producer = producer;
    _model = model;
    _color = color;
  }
}
