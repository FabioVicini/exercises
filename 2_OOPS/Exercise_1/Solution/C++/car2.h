#ifndef CAR2_H
#define CAR2_H

#include <iostream>

using namespace std;

namespace CarLibrary2 {

  class Car
  {
    private:
      string _producer;
      string _model;
      string _color;

    public:
      Car(const string& producer,
          const string& model,
          const string& color);

      string Show() { return _model + " (" + _producer + "): color " + _color; }

  };

  class ICarFactory
  {
    public:
      virtual Car* Create(const string& color) = 0;
  };

  class Ford : public ICarFactory
  {
    public:
      Car* Create(const string& color) { return new Car("Ford", "Mustang", color); }
  };

  class Toyota : public ICarFactory
  {
    public:
      Car* Create(const string& color) { return new Car("Toyota", "Prius", color); }
  };

  class Volkswagen : public ICarFactory
  {
    public:
      Car* Create(const string& color) { return new Car("Volkswagen", "Golf", color); }
  };
}

#endif // CAR2_H
