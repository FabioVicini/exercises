#ifndef __TEST_CAR2_H
#define __TEST_CAR2_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "car2.h"

using namespace testing;

TEST(TestCar2, TestShow)
{
  CarLibrary2::Car car = CarLibrary2::Car("Fiat", "Mustang", "Red");
  EXPECT_EQ(car.Show(), "Mustang (Fiat): color Red");
}

TEST(TestCarFactory2, TestCreateFord)
{
  CarLibrary2::Car* car = CarLibrary2::Ford().Create("Red");
  EXPECT_TRUE(car != nullptr);
  EXPECT_EQ(car != nullptr ? car->Show() : "", "Mustang (Ford): color Red");
  delete car;
}

TEST(TestCarFactory2, TestCreateToyota)
{
  CarLibrary2::Car* car = CarLibrary2::Toyota().Create("Red");
  EXPECT_TRUE(car != nullptr);
  EXPECT_EQ(car != nullptr ? car->Show() : "", "Prius (Toyota): color Red");
  delete car;
}

TEST(TestCarFactory2, TestCreateVolkswagen)
{
  CarLibrary2::Car* car = CarLibrary2::Volkswagen().Create("Red");
  EXPECT_TRUE(car != nullptr);
  EXPECT_EQ(car != nullptr ? car->Show() : "", "Golf (Volkswagen): color Red");
  delete car;
}

#endif // __TEST_CAR2_H
