class Car:
    def __init__(self, producer: str, model: str, color: str):
        pass

    def show(self) -> str:
        return ""


class ICarFactory:
    def create(self, color: str) -> Car:
        pass


class Ford(ICarFactory):
    def create(self, color: str) -> Car:
        return None


class Toyota(ICarFactory):
    def create(self, color: str) -> Car:
        return None


class Volkswagen(ICarFactory):
    def create(self, color: str) -> Car:
        return None
