class IShoppingApp:
    def numberElements(self) -> int:
        return 0

    def addElement(self, product: str):
        pass

    def undo(self):
        pass

    def reset(self):
        pass

    def searchElement(self, product: str) -> bool:
        return False


class VectorShoppingApp(IShoppingApp):
    def numberElements(self) -> int:
        return 0

    def addElement(self, product: str):
        raise Exception("Unimplemented method")

    def undo(self):
        raise Exception("Unimplemented method")

    def reset(self):
        raise Exception("Unimplemented method")

    def searchElement(self, product: str) -> bool:
        return False


class ListShoppingApp(IShoppingApp):
    def numberElements(self) -> int:
        return 0

    def addElement(self, product: str):
        raise Exception("Unimplemented method")

    def undo(self):
        raise Exception("Unimplemented method")

    def reset(self):
        raise Exception("Unimplemented method")

    def searchElement(self, product: str) -> bool:
        return False


class StackShoppingApp(IShoppingApp):
    def numberElements(self) -> int:
        return 0

    def addElement(self, product: str):
        raise Exception("Unimplemented method")

    def undo(self):
        raise Exception("Unimplemented method")

    def reset(self):
        raise Exception("Unimplemented method")

    def searchElement(self, product: str) -> bool:
        return False


class QueueShoppingApp(IShoppingApp):
    def numberElements(self) -> int:
        return 0

    def addElement(self, product: str):
        raise Exception("Unimplemented method")

    def undo(self):
        raise Exception("Unimplemented method")

    def reset(self):
        raise Exception("Unimplemented method")

    def searchElement(self, product: str) -> bool:
        return False


class HashShoppingApp(IShoppingApp):
    def numberElements(self) -> int:
        return 0

    def addElement(self, product: str):
        raise Exception("Unimplemented method")

    def undo(self):
        raise Exception("Unimplemented method")

    def reset(self):
        raise Exception("Unimplemented method")

    def searchElement(self, product: str) -> bool:
        return False
