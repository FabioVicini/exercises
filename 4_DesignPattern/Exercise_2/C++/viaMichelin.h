#ifndef VIAMICHELIN_H
#define VIAMICHELIN_H

#include <iostream>
#include <exception>

#include "bus.h"
#include <vector>

using namespace std;
using namespace BusLibrary;

namespace ViaMichelinLibrary {

  class BusStation : public IBusStation {
    public:
      BusStation(const string& busFilePath) { }

      void Load() { throw runtime_error("Unimplemented Method"); }
      int NumberBuses() const  { throw runtime_error("Unimplemented Method"); }
      const Bus& GetBus(const int& idBus) const { throw runtime_error("Unimplemented Method"); }
  };

  class MapData : public IMapData {
    public:
      MapData(const string& mapFilePath) {  }
      void Load() { throw runtime_error("Unimplemented Method"); }
      int NumberRoutes() const { throw runtime_error("Unimplemented Method"); }
      int NumberStreets() const { throw runtime_error("Unimplemented Method"); }
      int NumberBusStops() const { throw runtime_error("Unimplemented Method"); }
      const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const { throw runtime_error("Unimplemented Method"); }
      const Route& GetRoute(const int& idRoute) const { throw runtime_error("Unimplemented Method"); }
      const Street& GetStreet(const int& idStreet) const { throw runtime_error("Unimplemented Method"); }
      const BusStop& GetStreetFrom(const int& idStreet) const { throw runtime_error("Unimplemented Method"); }
      const BusStop& GetStreetTo(const int& idStreet) const { throw runtime_error("Unimplemented Method"); }
      const BusStop& GetBusStop(const int& idBusStop) const { throw runtime_error("Unimplemented Method"); }
  };

  class RoutePlanner : public IRoutePlanner {
    public:
      static int BusAverageSpeed;

    public:
      RoutePlanner(const IMapData& mapData,
                   const IBusStation& busStation) { }

      int ComputeRouteTravelTime(const int& idRoute) const { throw runtime_error("Unimplemented Method"); }
      int ComputeRouteCost(const int& idBus, const int& idRoute) const { throw runtime_error("Unimplemented Method"); }
  };

  class MapViewer : public IMapViewer {
    public:
      MapViewer(const IMapData& mapData) { }
      string ViewRoute(const int& idRoute) const { throw runtime_error("Unimplemented Method"); }
      string ViewStreet(const int& idStreet) const { throw runtime_error("Unimplemented Method"); }
      string ViewBusStop(const int& idBusStop) const { throw runtime_error("Unimplemented Method"); }
  };
}

#endif // VIAMICHELIN_H
